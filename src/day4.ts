import { sum } from './utils'

export class ScratchCard {
    id: number
    winningNumbers: Array<number>
    numbers: Array<number>

    constructor(id: number, winningNumbers: Array<number>, numbers: Array<number>) {
        this.id = id
        this.winningNumbers = winningNumbers
        this.numbers = numbers
    }

    matchingNumbers(): Array<number> {
        return this.numbers.filter((n) => this.winningNumbers.includes(n))
    }

    numberOfMatches(): number {
        return this.matchingNumbers().length
    }

    worthValue(): number {
        const size = this.matchingNumbers().length
        if (size == 0) return 0
        else return Math.pow(2, size - 1)
    }

    isWinner(): boolean {
        return this.numberOfMatches() != 0
    }
}

export function parseScratchCards(lines: Array<string>): Array<ScratchCard> {
    return lines.map((line) => {
        const initialParts = line.split(':')
        const numbers = initialParts[1].split('|')

        return new ScratchCard(
            +(initialParts[0].substring(5)),
            stringToNumberArray(numbers[0]),
            stringToNumberArray(numbers[1])
        )
    })
}

function stringToNumberArray(s: string): Array<number> {
    return s.trim().split(' ').map((n) => +n).filter((n) => n != 0)
}

export function worthValue(scratchCards: Array<ScratchCard>): number {
    return scratchCards
        .map((sc) => sc.worthValue())
        .reduce(sum)
}

export function numberOfResultingScratchCards(scratchCards: Array<ScratchCard>): number {
    const depths = new Array<number>(scratchCards.length)

    for (let i = scratchCards.length -1; i >= 0; i--) {
        const matches = scratchCards[i].numberOfMatches()
        const maxIndex = Math.min(scratchCards.length, i + matches)
        
        let depth = 1
        for (let j = i + 1; j <= maxIndex; j++) {
            depth = depth + depths[j]
        }
        depths[i] = depth
    }
    return depths.reduce(sum)
}
