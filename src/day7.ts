import { sum } from './utils'

export enum Card {
    Two = 2,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    Ace
}

export enum HandType {
    HighCard = 1,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind
}

export class Hand {
    cards: Array<Card>

    constructor(cards: Array<Card>) {
        if (cards.length != 5) throw new Error("Hands must contain 5 cards")
        this.cards = cards
    }

    rankType(jacksWild: boolean = false): HandType {
        return cardGroupToHandType(groupCards(this.cards), jacksWild)
    }
}

export class HandWithBid {
    hand: Hand
    bid: number
    
    constructor(hand: Hand, bid: number) {
        this.hand = hand
        this.bid = bid
    }
}

function groupCards(cards: Array<Card>): Map<Card, number> {
    const result = new Map<Card, number>()
    cards.forEach((card) => {
        const count = result.get(card)
        if (count === undefined) result.set(card, 1)
        else result.set(card, count + 1)
    })
    return result
}

function cardGroupToHandType(group: Map<Card, number>, jacksWild: boolean): HandType {
    if (jacksWild) return cardGroupToHandTypeJacksWild(group)
    else return cardGroupToHandTypeNoWildcards(group)
}

function cardGroupToHandTypeNoWildcards(group: Map<Card, number>): HandType {
    if (group.size == 1) return HandType.FiveOfAKind
    else if (group.size == 2 && [...group.values()].includes(4)) return HandType.FourOfAKind
    else if (group.size == 2) return HandType.FullHouse
    else if (group.size == 3 && [...group.values()].includes(3)) return HandType.ThreeOfAKind
    else if (group.size == 3) return HandType.TwoPair
    else if (group.size == 4) return HandType.OnePair
    else return HandType.HighCard
}

function cardGroupToHandTypeJacksWild(group: Map<Card, number>): HandType {
    const jackCount = extractJacks(group)
    const counts = [...group.values()]

    if (jackCount == 5) return HandType.FiveOfAKind
    else if (counts.includes(5 - jackCount)) return HandType.FiveOfAKind
    else if (counts.includes(4 - jackCount)) return HandType.FourOfAKind
    else if (counts.length == 2) return HandType.FullHouse
    else if (counts.includes(3 - jackCount)) return HandType.ThreeOfAKind
    else if (counts.length == 3) return HandType.TwoPair
    else if (counts.length == 4) return HandType.OnePair
    return HandType.HighCard
}

function extractJacks(group: Map<Card, number>): number {
    let jackCount = group.get(Card.Jack)
    if (jackCount === undefined) jackCount = 0
    group.delete(Card.Jack)
    return jackCount
}

export function sortHands(hands: Array<Hand>, jacksWild: boolean = false): Array<Hand> {
    return hands.sort(handPriority(jacksWild))
}

function handPriority(jacksWild: boolean): (left: Hand, right: Hand) => number {
    return (left, right) => {
        const handResult = left.rankType(jacksWild) - right.rankType(jacksWild) 
        if (handResult == 0) return cardPriority(left, right, jacksWild)
        else return handResult
    }
}

function cardPriority(left: Hand, right: Hand, jacksWild: boolean): number {
    for (let i = 0; i < left.cards.length; i++) {
        const cardResult = left.cards[i] - right.cards[i]

        // Custom rules for when jacks are wild and they become the lowest card in the pack
        if (jacksWild && cardResult != 0) {
            if (left.cards[i] == Card.Jack) return -1
            if (right.cards[i] == Card.Jack) return 1
        }

        if (cardResult != 0) return cardResult
    }
    return 0
}

export function calculateWinnings(handsWithBids: Array<HandWithBid>, jacksWild: boolean = false): number {
    return handsWithBids.sort((left, right) => handPriority(jacksWild)(left.hand, right.hand))
                        .map((item, index) => item.bid * (index + 1))
                        .reduce(sum)
}

export function parseHandsWithBids(lines: Array<string>): Array<HandWithBid> {
    return lines.map((line) => {
        const [cards, bid] = line.split(' ')
        return new HandWithBid(createHand(cards), +bid)
    })
}

function createHand(text: string): Hand {
    if (text.length != 5) throw new Error("Invalid hand specification")

    const cards: Array<Card> = []
    for (const c of text) {
        switch(c) {
            case '2': cards.push(Card.Two); break
            case '3': cards.push(Card.Three); break
            case '4': cards.push(Card.Four); break
            case '5': cards.push(Card.Five); break
            case '6': cards.push(Card.Six); break
            case '7': cards.push(Card.Seven); break
            case '8': cards.push(Card.Eight); break
            case '9': cards.push(Card.Nine); break
            case 'T': cards.push(Card.Ten); break
            case 'J': cards.push(Card.Jack); break
            case 'Q': cards.push(Card.Queen); break
            case 'K': cards.push(Card.King); break
            case 'A': cards.push(Card.Ace); break
            default: throw new Error(`Unknown card type: ${c}`)
        }
    }
    return new Hand(cards)
}
