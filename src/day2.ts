
export class Permutation {
    red: number
    green: number
    blue: number
    
    constructor(red: number, green: number, blue: number) {
        this.blue = blue
        this.red = red
        this.green = green
    }
}

export class GamePermutations {
    game: number
    attempts: Array<Permutation>

    constructor(game: number, attempts: Array<Permutation>) {
        this.game = game
        this.attempts = attempts
    }
}

export function parsePermutations(lines: Array<string>): Array<GamePermutations> {
    return lines.map(parsePermutation) 
}

function parsePermutation(line: string): GamePermutations {
    const parts = line.split(':')
    return new GamePermutations(
        +(parts[0].trim().substring(5)),
        parts[1].split(';').map(parseAttempt)
    )
}

function parseAttempt(attempt: string): Permutation {
    const colours = new Map<string, number>()
    attempt.split(',').forEach((s) => {
        const bits = s.trim().split(' ')
        const colour = bits[1].trim()
        const amount = +(bits[0].trim())
        colours.set(colour, amount)
    })
    return new Permutation( 
        numberOf('red', colours), 
        numberOf('green', colours), 
        numberOf('blue', colours) 
    )
}

function numberOf(colour: string, colours: Map<string, number>): number {
    const n = colours.get(colour)
    if (n === undefined) return 0
    else return n
}

export function sumOfValidGames(
    lines: Array<string>,
    maxRed: number,
    maxGreen: number,
    maxBlue: number): number {
    const games = lines.map(parsePermutation)
    return games.filter(allAttemptsValid(maxRed, maxGreen, maxBlue))
        .map((game) => game.game)
        .reduce((accumulator, value) => accumulator + value)
}

function allAttemptsValid(
    maxRed: number, 
    maxGreen: number, 
    maxBlue: number): (game: GamePermutations) => boolean {
    return (game) => 
        game.attempts.every((attempt) =>
            attempt.red <= maxRed &&
            attempt.green <= maxGreen &&
            attempt.blue <= maxBlue
        )
}

export function powerOfMinimumSets(lines: Array<string>): number {
    const games = lines.map(parsePermutation)
    return games.map(minimumSet)
         .map((p) => p.red * p.green * p.blue)
         .reduce((accumulator, value) => accumulator + value)
}

function minimumSet(game: GamePermutations): Permutation {
    let minRed: number = 0
    let minGreen: number = 0
    let minBlue: number = 0
    game.attempts.forEach((attempt) => {
        if (attempt.red > minRed) minRed = attempt.red
        if (attempt.green > minGreen) minGreen = attempt.green
        if (attempt.blue > minBlue) minBlue = attempt.blue
    })
    return new Permutation(minRed, minGreen, minBlue)
}
