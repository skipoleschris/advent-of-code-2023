import { sum } from './utils'

// Alternative approach

interface SpringRow {
    springs: string
    records: Array<number>
}

export function parseInput(lines: string[], expanded: boolean = false): Array<SpringRow> {
    return lines
        .map((line) => line.split(' '))
        .map(([springs, records]) => expand(springs, records, expanded))
        .map(([springs, records]) => buildSpringRow(springs, records))
}

function expand(springs: string, records: string, expanded: boolean): [string, string] {
    if (expanded) return [`${springs}?`.repeat(5).slice(0, -1), `${records},`.repeat(5).slice(0, -1)]
    else return [springs, records]
}

function buildSpringRow(springList: string, recordsList: string): SpringRow {
    const springs = springList
        .replace(/^\.+|\.+$/g, '')  // We can remove leading and trailing dots as they are meaningless
        .replace(/\.{2,}/g, '.')     // We can also remove double dots as they are meaningless too
    const records = recordsList.split(',').map(Number)
    return { springs, records }
}

export function countAllPermutations(rows: Array<SpringRow>): number {
    return rows.map((row) => countPermutations(row.springs, row.records)).reduce(sum)
}

const cache = new Map<string, number>()
const log = false

function cacheKey(springs: string, records: Array<number>): string {
    return `${springs}|${records.join(',')}`
}

export function countPermutations(springs: string, records: Array<number>, level: number = 0): number {
    const prefix = ' '.repeat(level)
    if (log) console.log(`${prefix} ${level} - countPermutations(${springs}, ${records})`)

    const key = cacheKey(springs, records)
    if (cache.has(key)) {
        if (log) console.log(`    Found in cache. ${key} -> ${cache.get(key)}`)
        return cache.get(key) as number
    }

    // All springs checked and no more record patterns to match?
    // return whether we are valid or invalid
    if (springs.length == 0) {
        if (log) console.log(`${prefix} ${level} -     No more springs to process. All records matched? ${records.length == 0}`)
        return records.length == 0 ? 1 : 0
    }

    let permutations = 0
    switch (springs.charAt(0)) {
        case '.': // Operational
            // Operational spring, so we can just continue to the next level
            permutations = countPermutations(springs.substring(1), records, level + 1)
            break
        case '?': // Unknown
            // Could be either, so try both operational and damaged
            permutations = countPermutations('.' + springs.substring(1), records, level + 1) +
                countPermutations('#' + springs.substring(1), records, level + 1)
            break
        default: // '#' - Damaged
            permutations = handleDamaged(springs, records, level)
            break
    }

    if (log) console.log(`${prefix} ${level} - Adding to cache ${key} = ${permutations}`)
    cache.set(key, permutations)
    return permutations
}

function handleDamaged(springs: string, records: Array<number>, level: number): number {
    const prefix = ' '.repeat(level)
    const numberRequiredToBeDamaged = records[0]
    if (log) console.log(`${prefix} ${level} - Checking to see if ${numberRequiredToBeDamaged} can be damaged...`)

    // If we can match the length of the damaged requirement, the following
    // must be true:
    // - The length of the springs must have enough characters
    // - The next n characters of the springs must all be damaged or unknown
    if (numberRequiredToBeDamaged <= springs.length && allCouldBeDamaged(springs, numberRequiredToBeDamaged)) {
        const newRecords = records.slice(1)
        if (numberRequiredToBeDamaged == springs.length) {
            // The damage block requirement exactly matches the number of springs
            // Check if there are reamaining groups or not so see if this was a valid permutation
            if (log) console.log(`${prefix} ${level} - Exact damage block match to end. All records matched? ${newRecords.length == 0}`)
            return newRecords.length == 0 ? 1 : 0
        }

        switch (springs.charAt(numberRequiredToBeDamaged)) {
            case '.': // Operational
                // The damage group is correctly sized. We can skip the operational one
                // and look for the next pattern
                return countPermutations(springs.substring(numberRequiredToBeDamaged + 1), newRecords, level + 1)
            case '?': // Unknown
                // The damaged group was found, so this unknown can only be an operational one
                return countPermutations("." + springs.substring(numberRequiredToBeDamaged + 1), newRecords, level + 1)
            default: // '#' - Damaged
                // Just matched the damaged group, so another damaged is invalid
                if (log) console.log(`${prefix} ${level} - Damaged after complete damaged block. Invalid permutation`)
                return 0
        }
    }
    else {
        // Can't match the damage record, so not a permutation
        if (log) console.log(`${prefix} ${level} - Not a sufficiently long block to match the number required to be damaged`)
        return 0
    }
}

function allCouldBeDamaged(springs: string, length: number): boolean {
    return [...(springs.slice(0, length))].every((spring) => spring == '#' || spring == '?')
}
