
export enum Direction {
    North,
    South,
    East,
    West
}

export class Position {
    row: number
    col: number
    
    constructor(row: number, col: number) {
        this.row = row
        this.col = col
    }

    move(direction: Direction): Position {
        switch (direction) {
            case Direction.North: return at(this.row - 1, this.col)
            case Direction.South: return at(this.row + 1, this.col)
            case Direction.East: return at(this.row, this.col + 1)
            case Direction.West: return at(this.row, this.col - 1)
        }
    }
}

function samePosition(left: Position, right: Position): boolean {
    return left.row == right.row && left.col == right.col
}

export function at(row: number, col: number): Position {
    return new Position(row, col)
}

export class Pipe {
    transitions: [Direction, Direction]

    constructor(first: Direction, second: Direction) {
        this.transitions = [ first, second ]
    }

    navigate(pipePosition: Position, comingFrom: Position): Position | undefined {
        const adjacents = this.transitions.map((direction) => pipePosition.move(direction))
        if (samePosition(adjacents[0], comingFrom)) return adjacents[1]
        else if (samePosition(adjacents[1], comingFrom)) return adjacents[0]
        else return undefined
    }
}

type StartItem = { start: boolean }
type GroundItem = { ground: boolean }
type PipeNetworkItem = Pipe | StartItem | GroundItem

export const pipeTypes = {
    horizontal: new Pipe(Direction.East, Direction.West),
    vertical: new Pipe(Direction.North, Direction.South),
    northEast: new Pipe(Direction.North, Direction.East),
    northWest: new Pipe(Direction.North, Direction.West),
    southEast: new Pipe(Direction.South, Direction.East),
    southWest: new Pipe(Direction.South, Direction.West),
    start: { start: true },
    ground: { ground: true }
}

export class PipeNetwork {
    pipes: Array<Array<PipeNetworkItem>>

    constructor(pipes: Array<Array<PipeNetworkItem>>) {
        this.pipes = pipes
    }

    startPosition(): Position {
        for (let row = 0; row < this.pipes.length; row++) {
            for (let col = 0; col < this.pipes[row].length; col++ ) {
                if (isStart(this.pipes[row][col])) return at(row, col)
            }
        }
        throw new Error("Pipe network doesn't have a start")
    }

    pipeAt(position: Position): Pipe | undefined {
        const pipe = this.pipes[position.row][position.col]
        if (isPipe(pipe)) return pipe
        else return undefined
    }

    allPositions(): Array<Position> {
        const positions: Array<Position> = []
        for (let row = 0; row < this.pipes.length; row++) {
            for (let col = 0; col < this.pipes[row].length; col++ ) {
                positions.push(at(row, col))
            }
        }
        return positions
    }
}

function isStart(value: PipeNetworkItem): value is StartItem {
    return value === pipeTypes.start
}

function isPipe(value: PipeNetworkItem): value is Pipe {
    return value === pipeTypes.horizontal || value === pipeTypes.vertical ||
           value === pipeTypes.northEast || value === pipeTypes.northWest ||
           value === pipeTypes.southEast || value === pipeTypes.southWest
}

export function constructPipeNetwork(lines: Array<string>): PipeNetwork {
    return new PipeNetwork(lines.map(toPipes))
}

function toPipes(line: string): Array<PipeNetworkItem> {
    const pipes: Array<PipeNetworkItem> = []
    for(const ch of line) {
        switch (ch) {
            case '|': pipes.push(pipeTypes.vertical); break
            case '-': pipes.push(pipeTypes.horizontal); break
            case 'L': pipes.push(pipeTypes.northEast); break
            case 'J': pipes.push(pipeTypes.northWest); break
            case '7': pipes.push(pipeTypes.southWest); break
            case 'F': pipes.push(pipeTypes.southEast); break
            case 'S': pipes.push(pipeTypes.start); break
            default: pipes.push(pipeTypes.ground); break
        }
    }
    return pipes
}

function findLoopPositions(network: PipeNetwork): Array<Position> {
    const start = network.startPosition()
    const loopStarts = findLoopStarts(start, network)

    const route: Array<Position> = [ start ]
    let last = start
    let current = loopStarts[0]
    while (!samePosition(current, start)) {
        route.push(current)

        const pipe = network.pipeAt(current)
        if (pipe === undefined) throw new Error("Invalid loop through pipe network")

        const next = pipe.navigate(current, last)
        if (next === undefined) throw new Error("Invalid loop through pipe network")

        last = current
        current = next
    }
    return route
}

export function findStepsToHalfWayThroughLoop(network: PipeNetwork): number {
    return findLoopPositions(network).length / 2
}

function findLoopStarts(startPosition: Position, network: PipeNetwork): Array<Position> {
    const allDirections = [ Direction.North, Direction.East, Direction.South, Direction.West ]

    return allDirections.reduce((accumulator: Array<Position>, direction) => {
        const position = startPosition.move(direction)
        const pipe = network.pipeAt(position)
        if (pipe !== undefined && pipe.transitions.includes(opposite(direction))) accumulator.push(position)
        return accumulator
    }, [])
}

function opposite(direction: Direction): Direction {
    switch (direction) {
        case Direction.North: return Direction.South
        case Direction.South: return Direction.North
        case Direction.East: return Direction.West
        case Direction.West: return Direction.East
    }
}

export function countEnclosedPositions(network: PipeNetwork): number {
    return findEnclosedPositions(network).length
}

function findEnclosedPositions(network: PipeNetwork): Array<Position> {
    const loopPositions = findLoopPositions(network)
    const positionsToCheck = allNonLoopPositions(network, loopPositions)

    const lookupMap = createLookupMap(loopPositions)
    return positionsToCheck.filter((position) => isPositionInsideLoop(position, lookupMap))
}

function allNonLoopPositions(network: PipeNetwork, loopPositions: Array<Position>): Array<Position> {
    const allPositions = network.allPositions()
    return allPositions.filter((position) => {
        for (const p of loopPositions) {
            if (samePosition(position, p)) return false
        }
        return true
    })
}

/*
    This is a generic solution for finding whether a given position is inside a
    polygon or not. This was grabed from the internet when I first implemented
    the part 2 solution. Hoewever, this is no longer used since I came up with
    my own algorithm based on the general principles of polygon traversal.
*/
function isPointInPolygon(position: Position, polygon: Array<Position>): boolean {
    const n = polygon.length
    let inside = false

    let p1 = polygon[0]
    for (let i = 0; i < n + 1; i++) {
        const p2 = polygon[i % n]        
        if (position.row > Math.min(p1.row, p2.row) && 
            position.row <= Math.max(p1.row, p2.row) && 
            position.col <= Math.max(p1.col, p2.col)) {
            if (p1.row != p2.row) {
                const xinters = (position.row - p1.row) * (p2.col - p1.col) / (p2.row - p1.row) + p1.col
                if (p1.col == p2.col || position.col < xinters) inside = !inside
            }
        }
        p1 = p2
    }
    return inside
}

/*
    This is my own solution to the polygon problem. It follows the standard ray-casting
    approach to solving the problem, but takes advantage of the fact that there are no
    diagonal lines in the problem space. All pipes are either horixontal, vertical or 
    90-degree bends.

    This algorithm works by ray-casting a line at a 45-degree angle towards the position
    we want to check. If the line passes through a position on the loop then we check the
    previous and next positions, to see if the line bisects these or not.

    Valid patterns that bisect are:
  
    \        \        \        \        
     \ #      \        \ #      \       
      \|       \        \|       \       
       X      #-X-#    #-X        X-#   
       |\        \        \       |\    
       # \        \        \      # \       
          \        \        \        \       

    Patterns that don't bisect, but just pass through a position, are:

    \        \
     \ #      \
      \|       \
       X-#    #-X
        \       |\
         \      # \
          \        \

    We can therefore find all the positions on the ray and see if any of those are in the
    set of positions making up the loop. For each of thoses positions we can then identify 
    the previous and next positions and then determine whether the ray position bisects
    a line or not. Each time we detect a bisect we flip the inside/outside flag
    to the opposite value.

    Note, to make this efficient the algorithm it first maps all the points on the loop into a lookup
    map which is a two-dimensional array where each point on the loop holds the positions of the
    previous and next position. Traversing the positions in the array is therefore just a simple
    array index operation rather than a slower search. 
*/
function isPositionInsideLoop(position: Position, lookupMap: [Position, Position][][]): boolean {
    let inside = false
    for (const rayPosition of calculateRayToPositions(position)) {
        const lookupRow = lookupMap[rayPosition.row]
        let position = undefined
        if (lookupRow !== undefined) position = lookupRow[rayPosition.col]
        if (position === undefined) continue

        // Ray crosses a position on the border of the loop
        const [ previous, next ] = position
        if (bisectsPoints(rayPosition, previous, next)) inside = !inside
    }

    return inside
}

function createLookupMap(loopPositions: Position[]): [Position, Position][][] {
    const result: [Position, Position][][] = []

    for (let i = 0; i < loopPositions.length; i++) {
        const position = loopPositions[i]
        let previous = loopPositions[i - 1]
        if (previous === undefined) previous = loopPositions[loopPositions.length - 1]
        let next = loopPositions[i + 1]
        if (next === undefined) next = loopPositions[0]

        if (result[position.row] === undefined) result[position.row] = [] 
        result[position.row][position.col] = [previous, next]
    }

    return result
}

function calculateRayToPositions(position: Position): Position[] {
    const result: Position[] = []
    let row = position.row - 1
    let col = position.col - 1
    while (row >= 0 && col >= 0) {
        result.push(at(row, col))
        row--
        col--
    }
    return result
}

function bisectsPoints(ray: Position, p1: Position, p2: Position): boolean {
    const p1AboveRay = (p1.row < ray.row && p1.col == ray.col) || (p1.row == ray.row && p1.col > ray.col)
    const p2AboveRay = (p2.row < ray.row && p2.col == ray.col) || (p2.row == ray.row && p2.col > ray.col)
    const p1BelowRay = (p1.row > ray.row && p1.col == ray.col) || (p1.row == ray.row && p1.col < ray.col)
    const p2BelowRay = (p2.row > ray.row && p2.col == ray.col) || (p2.row == ray.row && p2.col < ray.col)

    return (p1AboveRay && p2BelowRay) || (p1BelowRay && p2AboveRay)
}

export const exportedForTest = {
    findLoopStarts, findLoopPositions, findEnclosedPositions, allNonLoopPositions, isPointInPolygon
}
