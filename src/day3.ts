import { isDigit } from "./utils"

export class Position {
    row: number
    col: number

    constructor(row: number, col: number) {
        this.row = row
        this.col = col
    }

    samePosition(p: Position): boolean {
        return p.row == this.row && p.col == this.col
    }
}

export class Part {
    partNumber: number
    position: Position

    constructor(no: number, pos: Position) {
        this.partNumber = no
        this.position = pos
    }

    isTouching(component: Component): boolean {
        const adjacents = positionsAround(this.position, `${this.partNumber}`.length)
        return adjacents.some((p) => p.samePosition(component.position))
    }
}

export class Component {
    value: string
    position: Position

    constructor(val: string, pos: Position) {
        this.value = val
        this.position = pos
    }

    isGear(): boolean {
        return this.value == '*'
    }
}

export class Schematic {
    parts: Array<Part>
    components: Array<Component>

    constructor(parts: Array<Part>, components: Array<Component>) {
        this.parts = parts
        this.components = components
    }
}

export function parseSchematic(lines: Array<string>): Schematic {
    const contents = lines.map(parseLine)
        .reduce((accumulator, value) => {
            return [accumulator[0].concat(value[0]), accumulator[1].concat(value[1])]
        })
    return new Schematic(contents[0], contents[1])
}

function parseLine(line: string, index: number): [Array<Part>, Array<Component>] {
    const parts: Array<Part> = []
    const components: Array<Component> = []
    let digits: string = ''
    let digitsPos: Position | undefined = undefined
    for (let i = 0; i < line.length; i++) {
        const c = line.charAt(i)
        if (isDigit(c)) {
            digits = digits + c
            if (digitsPos === undefined) digitsPos = new Position(index + 1, i + 1)
        }
        else if (c == '.') {
            addPartIfPossible(parts, digits, digitsPos)
            digits = ''
            digitsPos = undefined
        }
        else { // must be a symbol
            addPartIfPossible(parts, digits, digitsPos)
            digits = ''
            digitsPos = undefined
            components.push(new Component(c, new Position(index + 1, i + 1 )))
        }
    }
    addPartIfPossible(parts, digits, digitsPos)

    return [parts, components]
}

function addPartIfPossible(
    parts: Array<Part>,
    digits: string,
    digitsPos: Position | undefined
) {
    if (digits.length > 0 && digitsPos !== undefined) {
        parts.push(new Part(+digits, digitsPos))
    }
}

// Algorithm functions

function positionsAround(position: Position, length: number): Array<Position> {
    const positions: Array<Position> = []

    for (let col = position.col - 1; col <= position.col + length; col++) {
        for (let row = position.row - 1; row <= position.row + 1; row++) {
            if (row < 1 || col < 1) continue;
            if (row == position.row && (col >= position.col && col < position.col + length)) continue 
            positions.push(new Position(row, col))
        }
    }
    return positions
}

export function sumValidPartNumbers(schematic: Schematic): number {
    return schematic.parts
      .filter((part) => isPartValid(part, schematic.components))
      .reduce((accumulator, part) => accumulator + part.partNumber, 0)
}

function isPartValid(part: Part, component: Array<Component>): boolean {
    return component.some((p) => part.isTouching(p))
}

export function sumGearRatios(schematic: Schematic): number {
    return schematic.components
        .map((component) => calculateGearRatio(component, schematic.parts))
        .reduce((accumulator, value) => accumulator + value)
}

function calculateGearRatio(component: Component, parts: Array<Part>): number {
    const touchingParts = parts.filter((part) => part.isTouching(component))
    if (touchingParts.length == 2) {
        return (touchingParts[0].partNumber * touchingParts[1].partNumber)
    }
    else return 0
}
