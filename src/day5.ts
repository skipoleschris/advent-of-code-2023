export class Almanac {
    seeds: Array<number>
    maps: Array<NumberMap>

    constructor (seeds: Array<number>, maps: Array<NumberMap>) {
        this.seeds = seeds
        this.maps = maps
    }

    locationOfSeed(seed: number): number {
        return this.maps.reduce((lastValue, map) => map.mappingFor(lastValue), seed)
    }

    lowestLocation(): number {
        return Math.min(...this.seeds.map((seed) => this.locationOfSeed(seed)))
    }

    seedAtLocation(location: number): number {
        return [...this.maps].reverse().reduce((lastValue, map) => map.reverseMappingFor(lastValue), location)
    }

    lowestRangeLocation(): number {
        let location = 0
        while (location < Number.MAX_SAFE_INTEGER) {
            const seed = this.seedAtLocation(location)
            if (isSeedInRange(seed, this.seeds)) break
            location++
        }
        return location
    }
}

function isSeedInRange(seed: number, seeds: Array<number>): boolean {
    for (let i = 0; i < seeds.length; i += 2) {
        const rangeStart = seeds[i]
        const rangeLength = seeds[i + 1]
        if (seed >= rangeStart && seed < rangeStart + rangeLength) return true
    }
    return false
}

export class NumberMap {
    entries: Array<NumberMapEntry>

    constructor(entries: Array<NumberMapEntry>) {
        this.entries = entries
    }

    mappingFor(source: number): number {
        const entry = this.entries.find((entry) => entry.isMapped(source))
        if (entry === undefined) return source
        else return entry.map(source)
    }

    reverseMappingFor(dest: number): number {
        const entry = this.entries.find((entry) => entry.isDestMapped(dest))
        if (entry === undefined) return dest
        else return entry.reverseMap(dest)
    }
}

export class NumberMapEntry {
    destinationStart: number
    sourceStart: number
    length: number

    constructor(destinationStart: number, sourceStart: number, length: number) {
        this.destinationStart = destinationStart
        this.sourceStart = sourceStart
        this.length = length
    }

    isMapped(source: number): boolean {
        return source >= this.sourceStart && source < this.sourceStart + this.length
    }

    map(source: number): number {
        if (!this.isMapped(source)) throw new Error("Unmapped source number")
        return this.destinationStart + (source - this.sourceStart)
    }

    isDestMapped(dest: number): boolean {
        return dest >= this.destinationStart && dest < this.destinationStart + this.length
    }

    reverseMap(dest: number): number {
        if (!this.isDestMapped(dest)) throw new Error("Unmapped dest number")
        return this.sourceStart + (dest - this.destinationStart)
    }
}

export function parseNumberMap(lines: Array<string>): NumberMap {
    lines.shift()
    const entries = lines.map((line) => {
        const numbers = line.trim().split(' ').map((x) => +x)
        return new NumberMapEntry(numbers[0], numbers[1], numbers[2])
    })
    return new NumberMap(entries)
}

export function parseAlmanac(lines: Array<string>): Almanac {
    const seeds = lines[0].substring(7).split(' ').map((x) => +x)
    const numberMaps = splitByBlankLines(lines.slice(2)).map(parseNumberMap)
    return new Almanac(seeds, numberMaps)
}

function splitByBlankLines(lines: Array<string>): Array<Array<string>> {
    const result: Array<Array<string>> = []
    let current: Array<string> = []
    for (const line of lines) {
        if (line == '') {
            result.push(current)
            current = []
        }
        else current.push(line)
    }
    result.push(current)
    return result
}
