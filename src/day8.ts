import { lcd } from './utils'

export enum Direction {
    Left,
    Right
}

export function* leftRightValues(seed: Array<Direction>): Generator<Direction, Direction, unknown> {
    let index = 0
    while(true) {
        yield seed[index]
        index++
        if (index == seed.length) index = 0
    }
}

export function traverse(nodeMap: Map<string, [string, string]>, node: string, direction: Direction): string {
    const options = nodeMap.get(node)
    if (options === undefined) throw new Error("Invalid node map")
    if (direction === Direction.Left) return options[0]
    else return options[1]
}

export function countSteps(from: string, to: string, nodeMap: Map<string, [string, string]>, directions: Array<Direction>): number {
    return countStepsUntil(from, nodeMap, directions, (node: string) => node == to)
}

function countStepsUntil(
    from: string, 
    nodeMap: Map<string, [string, string]>, 
    directions: Array<Direction>, 
    finished: (node: string) => boolean
): number {
    const gen = leftRightValues(directions)
    let count = 0
    let current = from
    while (!finished(current)) {
        current = traverse(nodeMap, current, gen.next().value)
        count++
    }
    return count
}

export function parseMap(lines: Array<string>): [Array<Direction>, Map<string, [string, string]>] {
    const directions = []
    for (const c of lines[0].trim()) {
        if (c == 'L') directions.push(Direction.Left)
        else directions.push(Direction.Right)
    }

    const nodeMap = new Map<string, [string, string]>()
    lines.slice(2).forEach((line) => {
        const key = line.substring(0, 3)
        const left = line.substring(7, 10)
        const right = line.substring(12, 15)
        nodeMap.set(key, [ left, right ])
    })

    return [ directions, nodeMap ]
}

// Part 2

export function countStepsConcurrent(nodeMap: Map<string, [string, string]>, directions: Array<Direction>): number {
    const startingNodes = [...nodeMap.keys()].filter((node) => node.endsWith('A'))

    // We can take a nice shortcut here because the input is arranged such that each starting node 
    // steps through to an ending node in exactly the same number of steps each time. E.g.:
    // 2 (+2) 4 (+2) 6 (+2) 8 and so on...
    // Therefore we only need to calculate an individual interval for each starting node.
    // This wouldn't be the case for more random data patterns that aren't designed specifically to
    // make this possible.
    const intervals = startingNodes.map((node) => countStepsUntil(node, nodeMap, directions, (node: string) => node.endsWith('Z')))

    return lcd(intervals)
}
