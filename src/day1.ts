import { isDigit } from "./utils"

export function part1Calibration(lines: Array<string>): number {
    return lines.map((line) => {
        let firstDigit: string = ''
        let lastDigit: string = ''
        for (const c of line) {
            if (isDigit(c)) {
                if (firstDigit == '') firstDigit = c
                lastDigit = c
            }
        }
        return +(firstDigit + lastDigit)
    }).reduce((result, x) => result + x, 0)
}

export function part2Calibration(lines: Array<string>): number {
    return lines.map((line) => {
        let firstDigit: string = ''
        let lastDigit: string = ''

        const lineForward = replaceWordsWithNumbers(line)
        for (const c of lineForward) {
            if (isDigit(c)) {
                firstDigit = c
                break
            }
        }

        const lineBackward = replaceWordsWithNumbersBackwards(line)
        for (let i = lineBackward.length - 1; i >= 0; i--) {
            const c = lineBackward.charAt(i)
            if (isDigit(c)) {
                lastDigit = c
                break
            }
        }

        return +(firstDigit + lastDigit)
    }).reduce((result, x) => result + x, 0)
}

type NumberTransformation = {
    text: string
    digit: string
}

const transformations: Array<NumberTransformation> = [
    { text: 'one',   digit: '1' },
    { text: 'two',   digit: '2' },
    { text: 'three', digit: '3' },
    { text: 'four',  digit: '4' },
    { text: 'five',  digit: '5' },
    { text: 'six',   digit: '6' },
    { text: 'seven', digit: '7' },
    { text: 'eight', digit: '8' },
    { text: 'nine',  digit: '9' }
]

function replaceWordsWithNumbers(line: string): string {
    let remainder: string = line
    let result: string = ''
    while (remainder.length > 0) {
        let transformed: boolean = false
        for(const transform of transformations) {
            if (remainder.startsWith(transform.text)) {
                remainder = remainder.substring(transform.text.length)
                result = result + transform.digit
                transformed = true
                break
            }
        }

        if (!transformed) {
            result = result + remainder.charAt(0)
            remainder = remainder.substring(1)
        }
    }
    
    return result
}

function replaceWordsWithNumbersBackwards(line: string): string {
    let remainder: string = line
    let result: string = ''
    while (remainder.length > 0) {
        let transformed: boolean = false
        for(const transform of transformations) {
            if (remainder.endsWith(transform.text)) {
                remainder = remainder.substring(0, remainder.length - transform.text.length)
                result = transform.digit + result
                transformed = true
                break
            }
        }

        if (!transformed) {
            result = remainder.charAt(remainder.length - 1) + result
            remainder = remainder.substring(0, remainder.length - 1)
        }
    }
    
    return result
}
