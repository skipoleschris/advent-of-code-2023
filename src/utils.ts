import { readFileSync } from 'fs'

export function readLines(day: number, part: number): Array<string> {
    const content = readFileSync(`./data/day_${day}_${part}.txt`, 'utf-8')
    return content.split("\n")
}

export function isDigit(c: string): boolean {
    return c >= '0' && c<= '9'
}

export function sum(left: number, right: number): number {
    return left + right
}

export function multiply(left: number, right: number): number {
    return left * right
}

function gcd(a: number, b: number): number {
    if (b == 0) return Math.abs(a)
    else return gcd(b, a % b)
}

function lcm(a: number, b: number): number {
    return Math.abs(a * b) / gcd(a, b)
}

export function lcd(numbers: Array<number>): number {
    let result = numbers[0]
    for (const number of numbers.slice(1)) {
        result = lcm(result, number)
    }
    return result
}

export function last<T>(items: Array<T>): T {
    return items[items.length - 1]
}

export function first<T>(items: Array<T>): T {
    return items[0]
}
