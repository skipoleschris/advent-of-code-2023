import { part1Calibration, part2Calibration } from '../day1'
import { readLines } from '../utils'

describe("Day 1 Test Cases", () => {

    test("Calculates calibration correctly", () => {
        const lines = [
            '1abc2',
            'pqr3stu8vwx',
            'a1b2c3d4e5f',
            'treb7uchet'
        ]

        expect(part1Calibration(lines)).toBe(142)
    })

    test('Calculates the calibration value for part 1', () => {
        const lines = readLines(1, 1)

        expect(part1Calibration(lines)).toBe(54940)
    })

    test("Calculates calibration containing words correctly", () => {
        const lines = [
            'two1nine',
            'eightwothree',
            'abcone2threexyz',
            'xtwone3four',
            '4nineeightseven2',
            'zoneight234',
            '7pqrstsixteen',
            '1testeightwo'
        ]

        expect(part2Calibration(lines)).toBe(293)
    })

    test('Calculates the calibration data for part 2', () => {
        const lines = readLines(1, 1)

        expect(part2Calibration(lines)).toBe(54208)
    })
})
