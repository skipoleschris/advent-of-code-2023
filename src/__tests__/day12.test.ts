import { readLines } from '../utils'
import { parseInput, countPermutations, countAllPermutations } from '../day12'

describe("Day 12 Test Cases", () => {

    test("The condition record can be parsed from the record file", () => {
        const rows = parseInput(sampleFile)

        expect(rows).toStrictEqual(sampleRows)
    })

    test("Identify the number of possible arrangements", () => {
        expect(countPermutations(sampleRows[0].springs, sampleRows[0].records)).toBe(1)
        expect(countPermutations(sampleRows[1].springs, sampleRows[1].records)).toBe(4)
        expect(countPermutations(sampleRows[2].springs, sampleRows[2].records)).toBe(1)
        expect(countPermutations(sampleRows[3].springs, sampleRows[3].records)).toBe(1)
        expect(countPermutations(sampleRows[4].springs, sampleRows[4].records)).toBe(4)
        expect(countPermutations(sampleRows[5].springs, sampleRows[5].records)).toBe(10)

         expect(countAllPermutations(sampleRows)).toBe(21)
    })

    test("Identify the total number of possible arrangements for part 1", () => {
        const lines = readLines(12, 1)
        const rows = parseInput(lines)

        expect(countAllPermutations(rows)).toBe(7653)
    })

    // Part 2

    test("The condition record can be parsed from the record file in expanded form", () => {
        const rows = parseInput(sampleFile, true)

        expect(rows).toStrictEqual(expandedSampleRows)
    })

    test("Identify the number of possible arrangements for an unfolded condition record", () => {
        expect(countPermutations(expandedSampleRows[0].springs, expandedSampleRows[0].records)).toBe(1)
        expect(countPermutations(expandedSampleRows[1].springs, expandedSampleRows[1].records)).toBe(16384)
        expect(countPermutations(expandedSampleRows[2].springs, expandedSampleRows[2].records)).toBe(1)
        expect(countPermutations(expandedSampleRows[3].springs, expandedSampleRows[3].records)).toBe(16)
        expect(countPermutations(expandedSampleRows[4].springs, expandedSampleRows[4].records)).toBe(2500)
        expect(countPermutations(expandedSampleRows[5].springs, expandedSampleRows[5].records)).toBe(506250)

        expect(countAllPermutations(expandedSampleRows)).toBe(525152)
    })

    test("Identify the total number of possible unfolded arrangements for part 2", () => {
        const lines = readLines(12, 1)
        const rows = parseInput(lines, true)

        expect(countAllPermutations(rows)).toBe(60681419004564)
    })

    const sampleFile = [
        '???.### 1,1,3',
        '.??..??...?##. 1,1,3',
        '?#?#?#?#?#?#?#? 1,3,1,6',
        '????.#...#... 4,1,1',
        '????.######..#####. 1,6,5',
        '?###???????? 3,2,1'
    ]

    const sampleRows = [
       { springs: '???.###', records: [ 1, 1, 3 ] },
       { springs: '??.??.?##', records: [ 1, 1, 3 ] },
       { springs: '?#?#?#?#?#?#?#?', records: [ 1, 3, 1, 6 ] },
       { springs: '????.#.#', records: [ 4, 1, 1 ] },
       { springs: '????.######.#####', records: [ 1, 6, 5 ] },
       { springs: '?###????????', records: [ 3, 2, 1 ] }
    ]

    const expandedSampleRows = [
        { springs: '???.###????.###????.###????.###????.###', records: [ 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 3 ] },
        { springs: '??.??.?##.?.??.??.?##.?.??.??.?##.?.??.??.?##.?.??.??.?##', records: [ 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 3 ] },
        { springs: '?#?#?#?#?#?#?#???#?#?#?#?#?#?#???#?#?#?#?#?#?#???#?#?#?#?#?#?#???#?#?#?#?#?#?#?', records: [ 1, 3, 1, 6, 1, 3, 1, 6, 1, 3, 1, 6, 1, 3, 1, 6, 1, 3, 1, 6 ] },
        { springs: '????.#.#.?????.#.#.?????.#.#.?????.#.#.?????.#.#', records: [ 4, 1, 1, 4, 1, 1, 4, 1, 1, 4, 1, 1, 4, 1, 1 ] },
        { springs: '????.######.#####.?????.######.#####.?????.######.#####.?????.######.#####.?????.######.#####', records: [ 1, 6, 5, 1, 6, 5, 1, 6, 5, 1, 6, 5, 1, 6, 5 ] },
        { springs: '?###??????????###??????????###??????????###??????????###????????', records: [ 3, 2, 1, 3, 2, 1, 3, 2, 1, 3, 2, 1, 3, 2, 1 ] }
    ]
})

