import { readLines } from '../utils'
import { Almanac, NumberMap, NumberMapEntry, parseNumberMap, parseAlmanac } from '../day5'

describe("Day 5 Test Cases", () => {

    test("A number map correctly maps numbers that are in a number range", () => {
        const map = new NumberMap([ new NumberMapEntry(50, 98, 2), new NumberMapEntry(52, 50, 48) ])

        expect(map.mappingFor(98)).toBe(50)
        expect(map.mappingFor(99)).toBe(51)
        expect(map.mappingFor(53)).toBe(55)

        expect(map.mappingFor(79)).toBe(81)
        expect(map.mappingFor(55)).toBe(57)
    })

    test("A number map handles numbers that aren't in a number range", () => {
        const map = new NumberMap([ new NumberMapEntry(50, 98, 2), new NumberMapEntry(52, 50, 48) ])

        expect(map.mappingFor(10)).toBe(10)

        expect(map.mappingFor(14)).toBe(14)
        expect(map.mappingFor(13)).toBe(13)
    })

    test("A number map can be parsed successfully", () => {
        const lines = [
            'soil-to-fertilizer map:',
            '0 15 37',
            '37 52 2',
            '39 0 15'
        ]

        expect(parseNumberMap(lines)).toStrictEqual(
            new NumberMap([
                new NumberMapEntry(0, 15, 37),
                new NumberMapEntry(37, 52, 2),
                new NumberMapEntry(39, 0, 15),
            ])
        )
    })

    test("The Almanac can be parsed correctly", () => {
        expect(parseAlmanac(sampleAlmanac)).toStrictEqual(
            new Almanac(
                [79, 14, 55, 13],
                [
                    new NumberMap([ new NumberMapEntry(50, 98, 2), new NumberMapEntry(52, 50,48) ]),
                    new NumberMap([ new NumberMapEntry(0, 15, 37), new NumberMapEntry(37, 52, 2), new NumberMapEntry(39, 0, 15) ]),
                    new NumberMap([ new NumberMapEntry(49, 53,8), new NumberMapEntry(0, 11, 42), new NumberMapEntry(42, 0,7), new NumberMapEntry(57, 7, 4) ]),
                    new NumberMap([ new NumberMapEntry(88, 18, 7), new NumberMapEntry(18, 25, 70) ]),
                    new NumberMap([ new NumberMapEntry(45, 77, 23), new NumberMapEntry(81, 45, 19), new NumberMapEntry(68, 64, 13) ]),
                    new NumberMap([ new NumberMapEntry(0, 69, 1), new NumberMapEntry(1, 0, 69) ]),
                    new NumberMap([ new NumberMapEntry(60, 56, 37), new NumberMapEntry(56, 93, 4) ])
                ]
            )
        )
    })

    test("Find the lowest location for the starting seed numbers", () => {
        const almanac = parseAlmanac(sampleAlmanac)

        expect(almanac.locationOfSeed(79)).toBe(82)
        expect(almanac.locationOfSeed(14)).toBe(43)
        expect(almanac.locationOfSeed(55)).toBe(86)
        expect(almanac.locationOfSeed(13)).toBe(35)

        expect(almanac.lowestLocation()).toBe(35)
    })

    test("Find the lowest location for the starting seed numbers in part 1 data", () => {
        const lines = readLines(5, 1)
        const almanac = parseAlmanac(lines)

        expect(almanac.lowestLocation()).toBe(31599214)
    })

    // Part 2 Tests

    test("A number map correctly reverse maps numbers that are in a number range", () => {
        const map = new NumberMap([ new NumberMapEntry(50, 98, 2), new NumberMapEntry(52, 50, 48) ])

        expect(map.reverseMappingFor(50)).toBe(98)
        expect(map.reverseMappingFor(51)).toBe(99)
        expect(map.reverseMappingFor(55)).toBe(53)

        expect(map.reverseMappingFor(81)).toBe(79)
        expect(map.reverseMappingFor(57)).toBe(55)
    })

    test("A number map handles reverse mapping of numbers that aren't in a number range", () => {
        const map = new NumberMap([ new NumberMapEntry(50, 98, 2), new NumberMapEntry(52, 50, 48) ])

        expect(map.reverseMappingFor(10)).toBe(10)

        expect(map.reverseMappingFor(14)).toBe(14)
        expect(map.reverseMappingFor(13)).toBe(13)
    })

    test("Find the seed number associated with a location", () => {
        const almanac = parseAlmanac(sampleAlmanac)

        expect(almanac.seedAtLocation(82)).toBe(79)
        expect(almanac.seedAtLocation(43)).toBe(14)
        expect(almanac.seedAtLocation(86)).toBe(55)
        expect(almanac.seedAtLocation(35)).toBe(13)
    })

    test("Find the lowest location for the starting seed number ranges", () => {
        const almanac = parseAlmanac(sampleAlmanac)

        expect(almanac.lowestRangeLocation()).toBe(46)
    })

    test("Find the lowest location for the starting seed number ranges in part 2 data", () => {
        const lines = readLines(5, 1)
        const almanac = parseAlmanac(lines)

        expect(almanac.lowestRangeLocation()).toBe(20358599)
    })

    const sampleAlmanac: Array<string> = [
        'seeds: 79 14 55 13',
        '',
        'seed-to-soil map:',
        '50 98 2',
        '52 50 48',
        '',
        'soil-to-fertilizer map:',
        '0 15 37',
        '37 52 2',
        '39 0 15',
        '',
        'fertilizer-to-water map:',
        '49 53 8',
        '0 11 42',
        '42 0 7',
        '57 7 4',
        '', 
        'water-to-light map:',
        '88 18 7',
        '18 25 70',
        '',
        'light-to-temperature map:',
        '45 77 23',
        '81 45 19',
        '68 64 13',
        '',
        'temperature-to-humidity map:',
        '0 69 1',
        '1 0 69',
        '',
        'humidity-to-location map:',
        '60 56 37',
        '56 93 4'
    ]
})