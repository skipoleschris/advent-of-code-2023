import { readLines } from '../utils'
import { Universe, at, buildUniverse } from '../day11'

describe("Day 11 Test Cases", () => {

    test("A universe can be expanded in the absense of galaxies", () => {
        const universe = new Universe([
            at(4,1), at(8,2), at(1,3), at(7,5), at(2,6),
            at(10,7), at(8,9), at(1,10), at(5,10)
        ])

        universe.expand(2)
        expect(universe.points).toStrictEqual([
            at(5,1), at(10,2), at(1,3), at(9,6), at(2,7),
            at(13,8), at(10,11), at(1,12), at(6,12)
        ])
    })

    test("The shortest path between one point and another can be found", () => {
        expect(at(2,7).shortestPathTo(at(6,12))).toBe(9)
        expect(at(10,11).shortestPathTo(at(5,1))).toBe(15)
        expect(at(1,3).shortestPathTo(at(13,8))).toBe(17)
        expect(at(6,12).shortestPathTo(at(1,12))).toBe(5)
    })

    test("The shortest paths between every combination of galaxy can be calculated", () => {
        const universe = new Universe([
            at(4,1), at(8,2), at(1,3), at(7,5), at(2,6),
            at(10,7), at(8,9), at(1,10), at(5,10)
        ])

        universe.expand(2)
        expect(universe.shortestPathBetweenAllGalaxyCombinations()).toBe(374)
    })

    test("A universe can be created from input data", () => {
        const lines = [
            '...#......',
            '.......#..',
            '#.........',
            '..........',
            '......#...',
            '.#........',
            '.........#',
            '..........',
            '.......#..',
            '#...#.....'
        ]

        expect(buildUniverse(lines)).toStrictEqual(new Universe([
            at(4,1), at(8,2), at(1,3), at(7,5), at(2,6),
            at(10,7), at(8,9), at(1,10), at(5,10)
        ]))
    })

    test("The shorted paths between all galaxy combinations can be calculated for part 1", () => {
        const lines = readLines(11, 1)
        const universe = buildUniverse(lines)
        universe.expand(2)

        expect(universe.shortestPathBetweenAllGalaxyCombinations()).toBe(10228230)
    })

    // Part 2

    test("The shortest paths between every combination of galaxy can be calculated when expanded by 10", () => {
        const universe = new Universe([
            at(4,1), at(8,2), at(1,3), at(7,5), at(2,6),
            at(10,7), at(8,9), at(1,10), at(5,10)
        ])

        universe.expand(10)
        universe.points.forEach((p) => console.log(`${p.x},${p.y}`))
        expect(universe.shortestPathBetweenAllGalaxyCombinations()).toBe(1030)
    })

    test("The shortest paths between every combination of galaxy can be calculated when expanded by 100", () => {
        const universe = new Universe([
            at(4,1), at(8,2), at(1,3), at(7,5), at(2,6),
            at(10,7), at(8,9), at(1,10), at(5,10)
        ])

        universe.expand(100)
        expect(universe.shortestPathBetweenAllGalaxyCombinations()).toBe(8410)
    })

    test("The shorted paths between all galaxy combinations can be calculated for part 2", () => {
        const lines = readLines(11, 1)
        const universe = buildUniverse(lines)
        universe.expand(1000000)

        expect(universe.shortestPathBetweenAllGalaxyCombinations()).toBe(447073334102)
    })
})
