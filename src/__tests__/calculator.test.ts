import { add, subtract, multiply, divide } from '../calculator'

describe("Calculation Test Cases", () => {

    test("Add 2 numbers", () => {
        const result = add(1, 2)
        expect(result).toBe(3)
    })

    test("Subtract 2 numbers", () => {
        const result = subtract(2, 1)
        expect(result).toBe(1)
    })

    test("Multiply 2 numbers", () => {
        const result = multiply(2, 3)
        expect(result).toBe(6)
    })

    test("Divide 2 numbers", () => {
        const result = divide(4, 2)
        expect(result).toBe(2)
    })
})