import { readLines } from '../utils'
import { GamePermutations, Permutation, parsePermutations, sumOfValidGames, powerOfMinimumSets } from '../day2'

describe("Day 2 Test Cases", () => {

    test("Parses permutations correctly", () => {
        const lines = [
            'Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green',
            'Game 33: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red'
        ]

        const permutations = parsePermutations(lines)
        expect(permutations[0]).toStrictEqual(
            new GamePermutations(1, [
                new Permutation(4, 0, 3),
                new Permutation(1, 2, 6),
                new Permutation(0, 2, 0)
            ])
        )
        expect(permutations[1]).toStrictEqual(
            new GamePermutations(33, [
                new Permutation(20, 8, 6),
                new Permutation(4, 13, 5),
                new Permutation(1, 5, 0)
            ])
        )
    })

    test("Sums the ids of games where all attemps are valid", () => {
        const lines = [
            'Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green',
            'Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue',
            'Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red',
            'Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red',
            'Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green'
        ]

        expect(sumOfValidGames(lines, 12, 13, 14)).toBe(8)
    })

    test('Calculates sum of valid games for part 1', () => {
        const lines = readLines(2, 1)

        expect(sumOfValidGames(lines, 12, 13, 14)).toBe(2563)
    })

    test("Calculates the power of all minimum sets of games", () => {
        const lines = [
            'Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green',
            'Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue',
            'Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red',
            'Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red',
            'Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green'
        ]

        expect(powerOfMinimumSets(lines)).toBe(2286)
    })

    test('Calculates the power of part 2', () => {
        const lines = readLines(2, 1)

        expect(powerOfMinimumSets(lines)).toBe(70768)
    })
})
