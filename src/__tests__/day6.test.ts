import { readLines } from '../utils'
import { Race, winningCombinations, parseRaceInfo, parseSingleRaceInfo } from '../day6'

describe("Day 6 Test Cases", () => {

    test("The distance travelled in a race can be calculated for each hold time", () => {
        const race = new Race(7, 9)

        expect(race.distanceTravelled(0)).toBe(0)        
        expect(race.distanceTravelled(1)).toBe(6)        
        expect(race.distanceTravelled(2)).toBe(10)        
        expect(race.distanceTravelled(3)).toBe(12)        
        expect(race.distanceTravelled(4)).toBe(12)        
        expect(race.distanceTravelled(5)).toBe(10)        
        expect(race.distanceTravelled(6)).toBe(6)        
        expect(race.distanceTravelled(7)).toBe(0)        
    })

    test("The number of ways to beat the current record can be calculated", () => {
        expect(new Race(7, 9).numberOfWaysToBeatCurrentRecord()).toBe(4)
        expect(new Race(15, 40).numberOfWaysToBeatCurrentRecord()).toBe(8)
        expect(new Race(30, 200).numberOfWaysToBeatCurrentRecord()).toBe(9)
    })

    test("The number of ways all records can be beaten can be calculated", () => {
        const races = [ new Race(7, 9), new Race(15, 40), new Race(30, 200) ]

        expect(winningCombinations(races)).toBe(288)
    })

    test("The races info can be parsed from input lines", () => {
        const lines = [
            'Time:      7  15   30',
            'Distance:  9  40  200'
        ]

        expect(parseRaceInfo(lines)).toStrictEqual([
            new Race(7, 9), new Race(15, 40), new Race(30, 200)
        ])
    })

    test("The total winning combinations can be calculated for part 1", () => {
        const lines = readLines(6, 1)
        const races = parseRaceInfo(lines)

        expect(winningCombinations(races)).toBe(1710720)
    })

    // Part 2

    test("The single race info can be parsed from the input lines", () => {
        const lines = [
            'Time:      7  15   30',
            'Distance:  9  40  200'
        ]

        expect(parseSingleRaceInfo(lines)).toStrictEqual(new Race(71530, 940200))
    })

    test("The number of ways a record can be beaten can be calculated", () => {
        const races = [ new Race(71530, 940200) ]

        expect(winningCombinations(races)).toBe(71503)
    })

    test("The total winning combinations can be calculated for part 2", () => {
        const lines = readLines(6, 1)
        const races = parseSingleRaceInfo(lines)

        expect(winningCombinations([ races ])).toBe(35349468)
    })
})
 