import { readLines } from '../utils'
import { ScratchCard, parseScratchCards, worthValue, numberOfResultingScratchCards } from '../day4'

describe("Day 4 Test Cases", () => {

    test("Parses the scratch card list correctly", () => {
        const scratchCards = parseScratchCards(sampleLines)

        expect(scratchCards[0]).toStrictEqual(scratchCard)
    })

    test("The matching numbers can be determined for a scratch card", () => {
        expect(scratchCard.matchingNumbers()).toStrictEqual([ 83, 86, 17, 48 ])
    })

    test("The worth value of a scratch card can be determined", () => {
        expect(scratchCard.worthValue()).toBe(8)
    })

    test("The total worth value of a set of scratch cards can be calculated", () => {
        const scratchCards = parseScratchCards(sampleLines)

        expect(worthValue(scratchCards)).toBe(13)
    })

    test('Calculates total worth value of scratch card for part 1', () => {
        const lines = readLines(4, 1)
        const scratchCards = parseScratchCards(lines)

        expect(worthValue(scratchCards)).toBe(20107)
    })

    // Part 2

    test('Calculates the number of ending scratch cards', () => {
        const scratchCards = parseScratchCards(sampleLines)

        expect(numberOfResultingScratchCards(scratchCards)).toBe(30)
    })

    test('Calculates the number of ending scratch cards for part 2', () => {
        const lines = readLines(4, 1)
        const scratchCards = parseScratchCards(lines)

        expect(numberOfResultingScratchCards(scratchCards)).toBe(8172507)
    })

    const sampleLines = [
        'Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53',
        'Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19',
        'Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1',
        'Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83',
        'Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36',
        'Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11'
    ]

    const scratchCard = new ScratchCard(1, [ 41, 48, 83, 86, 17 ], [ 83, 86, 6, 31, 17, 9, 48, 53 ])
})
