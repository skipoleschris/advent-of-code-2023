import { readLines, isDigit, sum, multiply, lcd, last, first } from '../utils'

describe("Utils Test Cases", () => {

    test("Read lines from sample file", () => {
        const lines = readLines(0, 1)
        expect(lines).toHaveLength(2)
        expect(lines[0]).toBe("Test")
        expect(lines[1]).toBe("Another Test")
    })

    test("Tell whether a string is a digit or not", () => {
        expect(isDigit('0')).toBe(true)
        expect(isDigit('1')).toBe(true)
        expect(isDigit('2')).toBe(true)
        expect(isDigit('3')).toBe(true)
        expect(isDigit('4')).toBe(true)
        expect(isDigit('5')).toBe(true)
        expect(isDigit('6')).toBe(true)
        expect(isDigit('7')).toBe(true)
        expect(isDigit('8')).toBe(true)
        expect(isDigit('9')).toBe(true)
        expect(isDigit('A')).toBe(false)
        expect(isDigit('-')).toBe(false)
    })

    test("Sum values together", () => {
        expect([ 1, 2, 3, 4, 5].reduce(sum)).toBe(15)
    })

    test("Multiply values tgether", () => {
        expect([ 1, 2, 3, 4, 5].reduce(multiply)).toBe(120)

    })

    test("We can calculate the lowest common denominator for a group of numbers", () => {
        expect(lcd([ 2, 4, 8 ])).toBe(8)
        expect(lcd([ 2, 5, 25 ])).toBe(50)
    })

    test("Get the last element of an array", () => {
        expect(last([ 1, 2, 3 ])).toBe(3)
    })

    test("Get the first element of an array", () => {
        expect(first([ 1, 2, 3 ])).toBe(1)
    })
})
