import { Direction, at, PipeNetwork, constructPipeNetwork, pipeTypes, findStepsToHalfWayThroughLoop, countEnclosedPositions, exportedForTest } from '../day10'
import { readLines } from '../utils'
const { findLoopStarts, findLoopPositions, findEnclosedPositions, allNonLoopPositions } = exportedForTest

describe("Day 10 Test Cases", () => {

    test("From any position we can move in a direction to a new position", () => {
        const position = at(3, 8)

        expect(position.move(Direction.North)).toStrictEqual(at(2, 8))
        expect(position.move(Direction.South)).toStrictEqual(at(4, 8))
        expect(position.move(Direction.East)).toStrictEqual(at(3, 9))
        expect(position.move(Direction.West)).toStrictEqual(at(3, 7))
    })

    test("A pipe network can be constructed", () => {
        expect(samplePipeNetwork).toStrictEqual(new PipeNetwork([
            [ pipeTypes.southWest, pipeTypes.horizontal, pipeTypes.southEast,  pipeTypes.southWest,  pipeTypes.horizontal ],
            [ pipeTypes.ground,    pipeTypes.southEast,  pipeTypes.northWest,  pipeTypes.vertical,   pipeTypes.southWest  ],
            [ pipeTypes.start,     pipeTypes.northWest,  pipeTypes.northEast,  pipeTypes.northEast,  pipeTypes.southWest  ],
            [ pipeTypes.vertical,  pipeTypes.southEast,  pipeTypes.horizontal, pipeTypes.horizontal, pipeTypes.northWest  ],
            [ pipeTypes.northEast, pipeTypes.northWest,  pipeTypes.ground,     pipeTypes.northEast,  pipeTypes.northWest  ]
        ]))
    })

    test("A pipe, given its position and the current creatue position, determines where the creature will go next", () => {
        expect(pipeTypes.vertical.navigate(at(3, 6), at(2, 6))).toStrictEqual(at(4, 6))
        expect(pipeTypes.vertical.navigate(at(3, 6), at(4, 6))).toStrictEqual(at(2, 6))
        expect(pipeTypes.vertical.navigate(at(3, 6), at(3, 5))).toBeUndefined()

        expect(pipeTypes.horizontal.navigate(at(3, 6), at(3, 5))).toStrictEqual(at(3, 7))
        expect(pipeTypes.horizontal.navigate(at(3, 6), at(3, 7))).toStrictEqual(at(3, 5))
        expect(pipeTypes.horizontal.navigate(at(3, 6), at(2, 6))).toBeUndefined()

        expect(pipeTypes.northEast.navigate(at(3, 6), at(2, 6))).toStrictEqual(at(3, 7))
        expect(pipeTypes.northEast.navigate(at(3, 6), at(3, 7))).toStrictEqual(at(2, 6))
        expect(pipeTypes.northEast.navigate(at(3, 6), at(4, 6))).toBeUndefined()

        expect(pipeTypes.northWest.navigate(at(3, 6), at(2, 6))).toStrictEqual(at(3, 5))
        expect(pipeTypes.northWest.navigate(at(3, 6), at(3, 5))).toStrictEqual(at(2, 6))
        expect(pipeTypes.northWest.navigate(at(3, 6), at(4, 6))).toBeUndefined()

        expect(pipeTypes.southEast.navigate(at(3, 6), at(4, 6))).toStrictEqual(at(3, 7))
        expect(pipeTypes.southEast.navigate(at(3, 6), at(3, 7))).toStrictEqual(at(4, 6))
        expect(pipeTypes.southEast.navigate(at(3, 6), at(2, 6))).toBeUndefined()

        expect(pipeTypes.southWest.navigate(at(3, 6), at(4, 6))).toStrictEqual(at(3, 5))
        expect(pipeTypes.southWest.navigate(at(3, 6), at(3, 5))).toStrictEqual(at(4, 6))
        expect(pipeTypes.southWest.navigate(at(3, 6), at(2, 6))).toBeUndefined()
    })

    test("The start positon of the pipe network can be obtained", () => {
        expect(samplePipeNetwork.startPosition()).toStrictEqual(at(2, 0))
    })

    test("Given a start position, the points that start the loop can be found", () => {
        expect(findLoopStarts(at(2, 0), samplePipeNetwork)).toStrictEqual([
            at(2, 1), at(3, 0)
        ])
    })

    test("Calculate the number of steps to the half way point in apipe network loop", () => {
        expect(findStepsToHalfWayThroughLoop(samplePipeNetwork)).toBe(8)
    })

    test("Calculate the steps for the part 1 problem", () => {
        const lines = readLines(10, 1)
        const network = constructPipeNetwork(lines)

        expect(findStepsToHalfWayThroughLoop(network)).toBe(7173)
    })

    const samplePipeNetwork = constructPipeNetwork([
        '7-F7-',
        '.FJ|7',
        'SJLL7',
        '|F--J',
        'LJ.LJ'
    ])

    // Part 2

    test("All the coordinate of a loop can be found", () => {
        expect(findLoopPositions(samplePipeNetwork)).toStrictEqual([
            at(2,0), at(2,1), at(1,1), at(1,2), at(0,2), at(0,3), at(1,3), at(2,3),
            at(2,4), at(3,4), at(3,3), at(3,2), at(3,1), at(4,1), at(4,0), at(3,0)
        ])
    })

    test("All the positions in the network can be found", () => {
        expect(samplePipeNetwork.allPositions()).toStrictEqual([
            at(0,0), at(0,1), at(0,2), at(0,3), at(0,4),
            at(1,0), at(1,1), at(1,2), at(1,3), at(1,4),
            at(2,0), at(2,1), at(2,2), at(2,3), at(2,4),
            at(3,0), at(3,1), at(3,2), at(3,3), at(3,4),
            at(4,0), at(4,1), at(4,2), at(4,3), at(4,4)
        ])
    })

    test("All the positions in the network that are not part of the loop can be found", () => {
        expect(allNonLoopPositions(samplePipeNetwork, findLoopPositions(samplePipeNetwork))).toStrictEqual([
            at(0,0), at(0,1), at(0,4),
            at(1,0), at(1,4),
            at(2,2),
            at(4,2), at(4,3), at(4,4)
        ])
    })

    test("Find all the coordinates fully enclosed by the loop", () => {
        const enclosed = findEnclosedPositions(samplePipeNetworkPart2)
        
        expect(enclosed).toStrictEqual([
            at(6, 2), at(6, 3), at(6, 6), at(6, 7)
        ])
    })

    test("Return the number of points fully enclosed by the loop", () => {
        expect(countEnclosedPositions(samplePipeNetworkPart2)).toBe(4)
    })

    test("Calculate the number of enclosed positions for part 2", () => {
        const lines = readLines(10, 1)
        const network = constructPipeNetwork(lines)

        expect(countEnclosedPositions(network)).toBe(291)
    })

    const samplePipeNetworkPart2 = constructPipeNetwork([
        '..........',
        '.S------7.',
        '.|F----7|.',
        '.||....||.',
        '.||....||.',
        '.|L-7F-J|.',
        '.|..||..|.',
        '.L--JL--J.',
        '..........'        
    ])
})