import { readLines } from '../utils'
import { Position, Schematic, Part, Component, parseSchematic, sumValidPartNumbers, sumGearRatios } from "../day3"

describe("Day 3 Test Cases", () => {

    test("Parses the engine schematic correctly", () => {
        const schematic = parseSchematic(sampleLines)
        expect(schematic).toStrictEqual(
            new Schematic(
                [
                    new Part(467, new Position(1, 1)),
                    new Part(114, new Position(1, 6)),
                    new Part(35, new Position(3, 3)),
                    new Part(633, new Position(3, 7)),
                    new Part(617, new Position(5, 1)),
                    new Part(58, new Position(6, 8)),
                    new Part(592, new Position(7, 3)),
                    new Part(755, new Position(8, 7)),
                    new Part(664, new Position(10, 2)),
                    new Part(598, new Position(10, 6)),
                ],
                [
                    new Component('*', new Position(2, 4)),
                    new Component('#', new Position(4, 7)),
                    new Component('*', new Position(5, 4)),
                    new Component('+', new Position(6, 6)),
                    new Component('$', new Position(9, 4)),
                    new Component('*', new Position(9, 6)),
                ]
            )
        )
    })

    test("Sums all of the valid part numbers", () => {
        const schematic = parseSchematic(sampleLines)
        expect(sumValidPartNumbers(schematic)).toBe(4361)
    })

    test('Calculates sum of valid part numbers for part 1', () => {
        const lines = readLines(3, 1)
        const schematic = parseSchematic(lines)

        expect(sumValidPartNumbers(schematic)).toBe(536202)
    })

    test("Sums all of the gear ratios", () => {
        const schematic = parseSchematic(sampleLines)
        expect(sumGearRatios(schematic)).toBe(467835)
    })

    test('Calculates sum of gear ratios part 2', () => {
        const lines = readLines(3, 1)
        const schematic = parseSchematic(lines)

        expect(sumGearRatios(schematic)).toBe(78272573)
    })

    const sampleLines = [
        '467..114..',
        '...*......',
        '..35..633.',
        '......#...',
        '617*......',
        '.....+.58.',
        '..592.....',
        '......755.',
        '...$.*....',
        '.664.598..'
    ]
})
