import { readLines } from '../utils'
import { exportedForTesting, sumNextReadings, parseReport, sumPreviousReadings } from '../day9'
const { reduceToDifferences, allZero, reduceToZeros, extendEachSequence } = exportedForTesting

describe("Day 9 Test Cases", () => {

    test("Reduce numbers to the difference between each value", () => {
        expect(reduceToDifferences([ 0, 3, 6, 9, 12, 15 ])).toStrictEqual([
            3, 3, 3, 3, 3
        ])
        expect(reduceToDifferences([ 10, 13, 16, 21, 30, 45 ])).toStrictEqual([
            3, 3, 5, 9, 15
        ])
    })

    test("Check if numbers are all zeros", () => {
        expect(allZero([ 0, 0, 0, 0 ])).toBe(true)
        expect(allZero([ 0, 0, 1, 0 ])).toBe(false)
    })

    test("Reduce the numbers repeatedly until all zeros", () => {
        expect(reduceToZeros([ 0, 3, 6, 9, 12, 15 ])).toStrictEqual([
            [ 0, 3, 6, 9, 12, 15 ],
            [ 3, 3, 3, 3, 3 ],
            [ 0, 0, 0, 0 ]
        ])

        expect(reduceToZeros([ 1, 3, 6, 10, 15, 21 ])).toStrictEqual([
            [ 1, 3, 6, 10, 15, 21 ],
            [ 2, 3, 4, 5,  6  ],
            [ 1, 1, 1, 1 ],
            [ 0, 0, 0 ]
        ])
    })

    test("Apply next and previoud value to each sequence", () => {
        const sequence1 = [
            [ 0, 3, 6, 9, 12, 15 ],
            [ 3, 3, 3, 3, 3 ],
            [ 0, 0, 0, 0 ]
        ]
        extendEachSequence(sequence1)
        expect(sequence1).toStrictEqual([
            [ -3, 0, 3, 6, 9, 12, 15, 18 ],
            [ 3, 3, 3, 3, 3, 3, 3 ],
            [ 0, 0, 0, 0, 0, 0 ]
        ])

        const sequence2 = [
            [ 1, 3, 6, 10, 15, 21 ],
            [ 2, 3, 4, 5,  6,  ],
            [ 1, 1, 1, 1 ],
            [ 0, 0, 0 ]
        ]
        extendEachSequence(sequence2)
        expect(sequence2).toStrictEqual([
            [ 0, 1, 3, 6, 10, 15, 21, 28 ],
            [ 1, 2, 3, 4, 5,  6, 7 ],
            [ 1, 1, 1, 1, 1, 1 ],
            [ 0, 0, 0, 0, 0 ]
        ])
    })

    test("Sum the next reading for each initial sequence", () => {
        expect(sumNextReadings([
            [ 0, 3, 6, 9, 12, 15 ],
            [ 1, 3, 6, 10, 15, 21 ],
            [ 10, 13, 16, 21, 30, 45 ]
        ])).toBe(114)
    })

    test("The puzzle input can be parsed", () => {
        expect(parseReport([
            '0 3 6 9 12 15',
            '1 3 6 10 15 21',
            '10 13 16 21 30 45'
        ])).toStrictEqual([
            [ 0, 3, 6, 9, 12, 15 ],
            [ 1, 3, 6, 10, 15, 21 ],
            [ 10, 13, 16, 21, 30, 45 ]
        ])
    })

    test("Produces a solution to part 1", () => {
        const lines = readLines(9, 1)
        const report = parseReport(lines)

        expect(sumNextReadings(report)).toBe(1789635132)
    })

    // Part 2

    test("Sum the previous reading for each initial sequence", () => {
        expect(sumPreviousReadings([
            [ 0, 3, 6, 9, 12, 15 ],
            [ 1, 3, 6, 10, 15, 21 ],
            [ 10, 13, 16, 21, 30, 45 ]
        ])).toBe(-3 + 0 + 5)
    })

    test("Produces a solution to part 2", () => {
        const lines = readLines(9, 1)
        const report = parseReport(lines)

        expect(sumPreviousReadings(report)).toBe(913)
    })
})
