import { readLines } from '../utils'
import { Direction, leftRightValues, traverse, countSteps, parseMap, countStepsConcurrent } from '../day8'

describe("Day 8 Test Cases", () => {

    test("A steam of left/right choices can be generated from an input pattern", () => {
        const gen = leftRightValues(sampleDirections)   
        
        expect(gen.next().value).toBe(Direction.Left)
        expect(gen.next().value).toBe(Direction.Left)
        expect(gen.next().value).toBe(Direction.Right)
        expect(gen.next().value).toBe(Direction.Left)
        expect(gen.next().value).toBe(Direction.Left)
        expect(gen.next().value).toBe(Direction.Right)
        expect(gen.next().value).toBe(Direction.Left)
    })

    test("Can traverse to the correct node given a direction", () => {
        expect(traverse(sampleNodeMap, 'BBB', Direction.Right)).toBe('ZZZ')
        expect(traverse(sampleNodeMap, 'BBB', Direction.Left)).toBe('AAA')
    })

    test("Counts the steps needed to get from AAA to ZZZ", () => {
        expect(countSteps('AAA', 'ZZZ', sampleNodeMap, sampleDirections)).toBe(6)
    })

    test("Parses the input to create the directions and the node map", () => {
        const lines = [
            'LLR',
            '',
            'AAA = (BBB, BBB)',
            'BBB = (AAA, ZZZ)',
            'ZZZ = (ZZZ, ZZZ)',
        ]

        expect(parseMap(lines)).toStrictEqual([ sampleDirections, sampleNodeMap ])
    })

    test("Counts the steps needed to get from AAA to ZZZ on the puzzle data for part 1", () => {
        const lines = readLines(8, 1)
        const [directions, nodeMap] = parseMap(lines)

        expect(countSteps('AAA', 'ZZZ', nodeMap, directions)).toBe(20569)
    })

    const sampleDirections = [ Direction.Left, Direction.Left, Direction.Right ]
    const sampleNodeMap = new Map<string, [string, string]>([
        [ 'AAA', [ 'BBB', 'BBB' ] ],
        [ 'BBB', [ 'AAA', 'ZZZ' ] ],
        [ 'ZZZ', [ 'ZZZ', 'ZZZ' ] ]
    ])

    // Part 2

    test("Concurrently navigates all paths from xxA to xxZ", () => {
        expect(countStepsConcurrent(sampleNodeMapPart2, sampleDirectionsPart2)).toBe(6)
    })

    test("Counts the steps needed to get from every xxA to xxZ on the puzzle data for part 2", () => {
        const lines = readLines(8, 1)
        const [directions, nodeMap] = parseMap(lines)

        expect(countStepsConcurrent(nodeMap, directions)).toBe(21366921060721)
    })

    const sampleDirectionsPart2 = [ Direction.Left, Direction.Right ]
    const sampleNodeMapPart2 = new Map<string, [string, string]>([
        [ '11A', [ '11B', 'XXX' ]],
        [ '11B', [ 'XXX', '11Z' ]],
        [ '11Z', [ '11B', 'XXX' ]],
        [ '22A', [ '22B', 'XXX' ]],
        [ '22B', [ '22C', '22C' ]],
        [ '22C', [ '22Z', '22Z' ]],
        [ '22Z', [ '22B', '22B' ]],
        [ 'XXX', [ 'XXX', 'XXX' ]],
    ])
})
