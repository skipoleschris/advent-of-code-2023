import { readLines } from '../utils'
import { Card, HandType, Hand, HandWithBid, sortHands, calculateWinnings, parseHandsWithBids } from '../day7'

describe("Day 7 Test Cases", () => {

    test("Hands can be matched to a single type", () => {
        expect(fiveOfAKindHand.rankType()).toBe(HandType.FiveOfAKind)
        expect(fourOfAKindHand.rankType()).toBe(HandType.FourOfAKind)
        expect(fullHouseHand.rankType()).toBe(HandType.FullHouse)
        expect(threeOfAKindHand.rankType()).toBe(HandType.ThreeOfAKind)
        expect(twoPairHand.rankType()).toBe(HandType.TwoPair)
        expect(onePairHand.rankType()).toBe(HandType.OnePair)
        expect(highCardHand.rankType()).toBe(HandType.HighCard)
    })

    test("Hands can be sorted based on their hand type", () => {
        const hands = [ onePairHand, threeOfAKindHand, highCardHand, fiveOfAKindHand, twoPairHand, fullHouseHand, fourOfAKindHand ]

        expect(sortHands(hands)).toStrictEqual([
            highCardHand, onePairHand, twoPairHand, threeOfAKindHand, fullHouseHand, fourOfAKindHand, fiveOfAKindHand
        ])
    })

    test("Hands with the same hand type are sorted by card value", () => {
        const hand1 = new Hand([ Card.Ace, Card.Ace, Card.Ace, Card.Three, Card.Two ])
        const hand2 = new Hand([ Card.Ace, Card.Ace, Card.Ace, Card.Two, Card.Three ])
        const hand3 = new Hand([ Card.Ace, Card.Ace, Card.Two, Card.Ace, Card.Three ])
        const hand4 = new Hand([ Card.Ace, Card.Two, Card.Ace, Card.Ace, Card.Three ])
        const hand5 = new Hand([ Card.Two, Card.Ace, Card.Ace, Card.Ace, Card.Three ])

        expect(sortHands([ hand5, hand4, hand2, hand1, hand3, hand4 ])).toStrictEqual([
            hand5, hand4, hand4, hand3, hand2, hand1
        ])
    })

    test("The winnings can be calculated based on hand rank and bid amount", () => {
        const handsWithBids = [
            new HandWithBid(new Hand([ Card.Three, Card.Two, Card.Ten, Card.Three, Card.King ]), 765),
            new HandWithBid(new Hand([ Card.Ten, Card.Five, Card.Five, Card.Jack, Card.Five ]), 684),
            new HandWithBid(new Hand([ Card.King, Card.King, Card.Six, Card.Seven, Card.Seven ]), 28),
            new HandWithBid(new Hand([ Card.King, Card.Ten, Card.Jack, Card.Jack, Card.Ten ]), 220),
            new HandWithBid(new Hand([ Card.Queen, Card.Queen, Card.Queen, Card.Jack, Card.Ace ]), 483)
        ]

        expect(calculateWinnings(handsWithBids)).toBe(6440)
    })

    test("The puzzle input can be parsed from a list of lines", () => {
        const lines = [
            '32T3K 765',
            'T55J5 684',
            'KK677 28',
            'KTJJT 220',
            'QQQJA 483'
        ]

        const handsWithBids = [
            new HandWithBid(new Hand([ Card.Three, Card.Two, Card.Ten, Card.Three, Card.King ]), 765),
            new HandWithBid(new Hand([ Card.Ten, Card.Five, Card.Five, Card.Jack, Card.Five ]), 684),
            new HandWithBid(new Hand([ Card.King, Card.King, Card.Six, Card.Seven, Card.Seven ]), 28),
            new HandWithBid(new Hand([ Card.King, Card.Ten, Card.Jack, Card.Jack, Card.Ten ]), 220),
            new HandWithBid(new Hand([ Card.Queen, Card.Queen, Card.Queen, Card.Jack, Card.Ace ]), 483)
        ]

        expect(parseHandsWithBids(lines)).toStrictEqual(handsWithBids)
    })

    test("The winnings can be calculated from the puzzle input for part 1", () => {
        const lines = readLines(7, 1)
        const handsWithBids = parseHandsWithBids(lines)
        const winnings = calculateWinnings(handsWithBids)

        expect(winnings).toBe(250602641)
    })

    // Part 2 Tests

    test("Jacks are wild and can uprate a hand type", () => {
        expect(new Hand([ Card.Ace, Card.Ace, Card.Jack, Card.Ace, Card.Jack ]).rankType(true)).toBe(HandType.FiveOfAKind)        
        expect(new Hand([ Card.Ace, Card.Jack, Card.Jack, Card.Ace, Card.Two ]).rankType(true)).toBe(HandType.FourOfAKind)        
        expect(new Hand([ Card.Ace, Card.Jack, Card.Two, Card.Ace, Card.Two ]).rankType(true)).toBe(HandType.FullHouse)        
        expect(new Hand([ Card.Ace, Card.Jack, Card.Two, Card.Jack, Card.Three ]).rankType(true)).toBe(HandType.ThreeOfAKind)        
        expect(new Hand([ Card.Ace, Card.Jack, Card.Two, Card.Three, Card.Four ]).rankType(true)).toBe(HandType.OnePair)        
    })

    test("Jacks are counted at the lowest value when ordering hands of the same type", () => {
        const hand1 = new Hand([ Card.Three, Card.Three, Card.Three, Card.Two, Card.Two])
        const hand2 = new Hand([ Card.Three, Card.Three, Card.Jack, Card.Two, Card.Two])
        const hand3 = new Hand([ Card.Jack, Card.Three, Card.Two, Card.Three, Card.Two])

        expect(sortHands([ hand2, hand3, hand1 ], true)).toStrictEqual([
            hand3, hand2, hand1
        ])
    })

    test("The winnings can be calculated based on hand rank and bid amount when jacks are wild", () => {
        const handsWithBids = [
            new HandWithBid(new Hand([ Card.Three, Card.Two, Card.Ten, Card.Three, Card.King ]), 765),
            new HandWithBid(new Hand([ Card.Ten, Card.Five, Card.Five, Card.Jack, Card.Five ]), 684),
            new HandWithBid(new Hand([ Card.King, Card.King, Card.Six, Card.Seven, Card.Seven ]), 28),
            new HandWithBid(new Hand([ Card.King, Card.Ten, Card.Jack, Card.Jack, Card.Ten ]), 220),
            new HandWithBid(new Hand([ Card.Queen, Card.Queen, Card.Queen, Card.Jack, Card.Ace ]), 483)
        ]

        expect(calculateWinnings(handsWithBids, true)).toBe(5905)
    })

    test("The winnings can be calculated from the puzzle input for part 2", () => {
        const lines = readLines(7, 1)
        const handsWithBids = parseHandsWithBids(lines)
        const winnings = calculateWinnings(handsWithBids, true)

        expect(winnings).toBe(251037509)
    })

    const fiveOfAKindHand = new Hand([ Card.Ace, Card.Ace, Card.Ace, Card.Ace, Card.Ace ])
    const fourOfAKindHand = new Hand([ Card.Ace, Card.Ace, Card.Eight, Card.Ace, Card.Ace ])
    const fullHouseHand = new Hand([ Card.Two, Card.Three, Card.Three, Card.Three, Card.Two ])
    const threeOfAKindHand = new Hand([ Card.Ten, Card.Ten, Card.Ten, Card.Nine, Card.Eight ])
    const twoPairHand = new Hand([ Card.Two, Card.Three, Card.Four, Card.Three, Card.Two ])
    const onePairHand = new Hand([ Card.Ace, Card.Two, Card.Three, Card.Ace, Card.Four ])
    const highCardHand = new Hand([ Card.Two, Card.Three, Card.Four, Card.Five, Card.Six ])
})
