import { sum, last, first } from "./utils";

function reduceToDifferences(numbers: Array<number>): Array<number> {
    if (numbers.length < 2) throw new Error("Numbers too short to reduce")

    const result: Array<number> = []
    let lastNumber: number | undefined = undefined
    numbers.forEach((number) => {
        if (lastNumber !== undefined)  result.push(number - lastNumber)
        lastNumber = number
    })
    return result
}

function allZero(numbers: Array<number>): boolean {
    return numbers.every((x) => x == 0)
}

function reduceToZeros(numbers: Array<number>): Array<Array<number>> {
    if (allZero(numbers)) return [numbers]
    else return [numbers].concat(reduceToZeros(reduceToDifferences(numbers)))
}

// This implmentation expands both the next and previous values in each sequence at
// the same time as this is easy to do in a single traverse
function extendEachSequence(sequences: Array<Array<number>>) {
    const lastIndex = sequences.length - 1
    padWith(sequences[lastIndex], 0, 0)
    for (let i = lastIndex - 1; i >= 0; i--) {
        const currentSequence = sequences[i]
        const lowerSequence = sequences[i + 1]
        padWith(currentSequence, first(currentSequence) - first(lowerSequence), last(currentSequence) + last(lowerSequence))
    }
}

function padWith(sequence: Array<number>, before: number, after: number) {
    sequence.unshift(before)
    sequence.push(after)
}

export function sumNextReadings(numbers: Array<Array<number>>): number {
    return sumReadings(numbers, last)
}

export function sumPreviousReadings(numbers: Array<Array<number>>): number {
    return sumReadings(numbers, first)
}

function sumReadings(numbers: Array<Array<number>>, selectFrom: (items: Array<number>) => number): number {
    const sequences = numbers.map((sequence) => reduceToZeros(sequence))
    sequences.forEach(extendEachSequence)
    return sequences.map((sequence) => selectFrom(sequence[0])).reduce(sum)
}

export function parseReport(lines: Array<string>): Array<Array<number>> {
    return lines.map((line) => line.trim().split(' ').map((x) => +x))
}

export const exportedForTesting = {
    reduceToDifferences, allZero, reduceToZeros, extendEachSequence
}
