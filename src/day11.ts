
export class Point {
    x: number
    y: number

    constructor(x: number, y: number) {
        this.x = x
        this.y = y
    }

    shiftRight(by: number) {
        this.x += by
    }

    shiftDown(by: number) {
        this.y += by
    }

    shortestPathTo(point: Point): number {
        return Math.abs(point.x - this.x) + Math.abs(point.y - this.y)
    }
}

export function at(x: number, y: number): Point {
    return new Point(x, y)
}

export class Universe {
    points: Array<Point>

    constructor(points: Array<Point>) {
        this.points = points
    }

    expand(expansionFactor: number) {
        this.expandInDirection(expansionFactor, (p) => p.x, (p, n) => p.shiftRight(n))
        this.expandInDirection(expansionFactor, (p) => p.y, (p, n) => p.shiftDown(n))
    }

    private expandInDirection(
        expansionFactor: number, 
        extractDirection: (p: Point) => number,
        movePoint: (p: Point, amount: number) => void
    ) {
        let max = Math.max(...this.points.map(extractDirection))
        let index = 1
        while (index <= max) {
            if (this.hasGalaxyInPosition(index, extractDirection)) index++
            else {
                max += (expansionFactor - 1)
                this.points.forEach((p) => { if (extractDirection(p) > index) movePoint(p, expansionFactor - 1) })
                index += expansionFactor
            }
        }
    }

    private hasGalaxyInPosition(index: number, extractDirection: (p: Point) => number): boolean {
        return this.points.find((p) => extractDirection(p) == index) !== undefined
    }

    shortestPathBetweenAllGalaxyCombinations(): number {
        let pathLength = 0
        for (let source = 0; source < this.points.length; source++) {
            for (let dest = source + 1; dest < this.points.length; dest++) {
                pathLength += this.points[source].shortestPathTo(this.points[dest])
            }
        }
        return pathLength
    }
}

export function buildUniverse(lines: Array<string>): Universe {
    const points: Array<Point> = []
    lines.forEach((line, lineIndex) => {
        [...line].forEach((ch, characterIndex) => {
            if (ch == '#') points.push(at(characterIndex + 1, lineIndex + 1))
        })
    })
    return new Universe(points)
}