import { add } from './calculator'
import { sumFileContents } from './summer'

console.log("Hello World!")
console.log("2 + 3 = " + add(2, 3))
console.log("Sum of contents: " + sumFileContents())
console.log("Finished!")
