import { readFileSync } from 'fs'

export function sumFileContents() {
    const content = readFileSync('./data/sample.txt', 'utf-8')
    return content.split("\n")
                  .map((x) => +x)
                  .reduce((accumulator, value) => accumulator + value)
}
