import { isDigit, multiply } from "./utils"

export class Race {
    duration: number
    record: number

    constructor(duration: number, record: number) {
        this.duration = duration
        this.record = record
    }

    distanceTravelled(holdDuration: number): number {
        if (holdDuration < 0 || holdDuration > this.duration) {
            throw new Error('Invalid hold time')
        }
        const speed = holdDuration
        const moveDuration = this.duration - holdDuration
        return speed * moveDuration
    }

    /*
        This was my original solution, but it iterates every hold duration and that's
        not needed, so I wrote a faster version.

        numberOfWaysToBeatCurrentRecord(): number {
            let count = 0
            for (let holdDuration = 1; holdDuration < this.duration; holdDuration++) {
                if (this.distanceTravelled(holdDuration) > this.record) count++
            }
            return count
        }
    */

    numberOfWaysToBeatCurrentRecord(): number {
        let lowerBound = 1
        while (this.distanceTravelled(lowerBound) <= this.record) lowerBound++

        let upperBound = this.duration - 1
        while (this.distanceTravelled(upperBound) <= this.record) upperBound--

        return upperBound - lowerBound + 1
    }
}

export function winningCombinations(races: Array<Race>): number {
    return races.map((race) => race.numberOfWaysToBeatCurrentRecord()).reduce(multiply)
}

export function parseRaceInfo(lines: Array<string>): Array<Race> {
    const times = extractNumbers(lines[0])
    const distances = extractNumbers(lines[1])
    return times.map((time, index) => new Race(time, distances[index]))
}

function extractNumbers(line: string): Array<number> {
    const result: Array<number> = []
    let current: string = ''
    for (const ch of line) {
        if (isDigit(ch)) current = current + ch
        else {
            if (current.length > 0) result.push(+current)
            current = ''
        }
    }
    if (current.length > 0) result.push(+current)
    return result
}

export function parseSingleRaceInfo(lines: Array<string>): Race {
    const time =  +(lines[0].substring(9).replaceAll(' ', ''))
    const distance = +(lines[1].substring(9).replaceAll(' ', ''))
    return new Race(time, distance)
}
