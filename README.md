# Advent of Code 2023

See https://adventofcode.com/ for full details.

This year I'm working for a new company that have an extensive NodeJS and Typescript codebase.
I'm therefore solving all the problems using this stack in order to refresh my skillset in this
area.

I try to solve all the problems with a test-first approach, meaning that each day's solution
comes with a bunch of unit tests.

## Some General Observations

While solving the problems, I've been collecting some useful observations and hints about what
is working best for me. 

### TDD at a more granular level than the problem

When I started working on these, my unit tests typically contained four test cases:

- Part 1 solution with worked example
- Part 1 solution with puzzle input
- Part 2 solution with worked example
- Part 2 solution with puzzle input

I then started separting the puzzle input parsing into a separate function, with its own test
case, so that the algorithm functions just work on pure parsed data.

However, what I was finding was that my algorithms often had subtle bugs (range errors, off by
one etc.) that were taking a lot of time to track down via logging/debugging. Since realising this,
I've switched to a much more granular TDD approach, building my algorithms from much smaller, 
well tested functions. This has significantly reduced the number of algorithm bugs to the point
where my part 1 solutions usually work first time.

### Look for patterns in the puzzle input data

In the early stages of Advent of Code, there usually aren't any tricky problems or data sets to
work with. Normally you can write some code that solves the worked examples and then just apply this
to the puzzle input without any issues. The solution space is usually small enough that any brute
force approach works just fine.

However, as the days progress, the solution spaces become larger and often the part 2 solutions can
no longer be solved with a brute force algorithm. It then becomes necessary to look for patterns
in the puzzle data that you can exploit. 

When it looks like the solution would require a brute force effort against a big solution space,
then don't waste time writing that algorithm. Instead spend effort exploring the data, printing out
intermediate values and so forth. Look for any patterns or progressions in the data values that
would allow avoiding writing brute force algorithms.

### Solve just the specific problem, not the more general one

The other thing I spotted (related to the input data) is that often my initial thought is to usually create a
solution that works across the entire input space. This is probably down to years of experience problem
solving and looking for edge cases. I'm just subconsciously thinking about how the algorithm will work
given this input or that input.

It's important to bear in mind that these problems are carefully curated so that they can be efficiently
solved against the supplied puzzle input. It's just not necessary to create a more general algorithm that
can work with anything other than the specific puzzle input supplied.

### Convergence problems

A number of problems often end up being some form of convergence problem. These are usually in the form
of a number of separate calculations that can repeat over and over. The solution is usually the first 
occurrence where all of the calculations reach a specific state at the same time. To prevent brute force
solutions, these often requite trillions of iterations before a convergence occurs.

These problems usually end up having a fixed repeating interval ot n-th term formula for each calculation, 
so the answer for these can usually be found by obtaining the lowest common denominator of all the interval values.
